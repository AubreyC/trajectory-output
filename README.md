# Trajectories output

## Reference frame

The reference frame used is defined during the camera calibration:

- Axis: North East Down
- Origin: Defined during the calibration process

## Detection zone

The detection zone, which corresponds to the area in wich the vehciles are tracked is defined by the red polygone.

- Brest (Belarus): (52.104134, 23.786564)

![alt text](images/varna_sat_view.png)
![alt text](images/varna_camera_view.png)

- Varna (Bulgaria): (43.212559, 27.904004)

![alt text](images/brest_sat_view.png)
![alt text](images/brest_camera_view.png)

## Sequences

Videos are processed by slices of fixed duration called sequences:

- `track_id` are unique inside a sequence
- `timestamp_ms` corresponds to the time ellapsed since the start of the sequence

## Format

Each sequence is composed of:

- `XXXXX_meta.csv`: Contains information about the sequence (e.g date, duration, etc)
- `XXXXX_trajectories.csv`: Actual trajectory data
- `HD_maps`: HP maps of the area in `OSM` format

### Meta csv

Field name | Unit | Description
---| ---| ---
location_name | No unit | Name of the location
date | YYYYMMDD | Date of the sequence
start_time | HHMM | Starting time of the sequence
duration_s | Seconds | Duration of the sequence in seconds
total_trajectories | No unit | Total number of trajectories
fps | Hz | Frame per seconds

### Trajectories csv

The file `XXXXXXXX_pandas_traj.csv` contains the extracted trajectories. The fields are described in the following table:

Field name | Unit | Description
---| ---| ---
track_id | No unit | Id of the trajectory
frame_id | No unit | Id of the frame
timestamp_s | Seconds | Time ellapsed since the begining of the sequence.
agent_type | No unit | Type of agent (car, trucks, bus, etc)
x | Meters | Position of the geometrical center of the agent along the X axis, in the scene reference frame
y | Meters | Position of the geometrical center of the agent along the Y axis, in the scene reference frame
vx | Meters / seconds | Velocity of the geometrical center of the agent along the X axis, in the scene reference frame
vy | Meters / seconds | Velocity of the geometrical center of the agent along the Y axis, in the scene reference frame
psi_rad | Radians | Heading of the vehicle in the scene reference frame
length | Meters | Approximate length of the vehicle
width | Meters | Approximate width of the vehicle

**Note:** Vehicle's size (length, width) are predifined by type of vehicles, which means that they are rought estimate and should not be used as accurate values.