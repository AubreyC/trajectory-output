# Camera config viewer

## Setup

Create virtual environment:

    virtualenv -p python3 ve3_trajectory-output

Activate it:

    source ve3_trajectory-output/bin/activate

Install setup:

    pip install -e .

## Run the viewer

To create the HD maps, use the Camera view as detections are done from this point of view.

For Brest (Belarus): (52.104134, 23.786564):

    python run_show_calib.py -c brest/brest_area1_street_cfg.yml -i brest/brest_area1_street.jpg -d brest/brest_area1_detection_zone.yml

For Varna (Bulgaria): (43.212559, 27.904004):

    python run_show_calib.py -c brest/brest_area1_street_cfg.yml -i brest/brest_area1_street.jpg -d brest/brest_area1_detection_zone.yml
