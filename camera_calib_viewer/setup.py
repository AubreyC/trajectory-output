from setuptools import setup

setup(name="camera-calib-viewer",
      version='1.0b',
      install_requires=['numpy',
                        'opencv-python',
                        'opencv-contrib-python',
                        'PyYAML',
                        'matplotlib',
                        'pytest',
                        'scipy'])
